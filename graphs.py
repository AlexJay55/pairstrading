﻿import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
from statsmodels.tsa.johansen import coint_johansen
style.use('ggplot')
from mpl_toolkits.mplot3d import axes3d
import statsmodels.formula.api as sm
#from Pairs import get_cum_log_ret
#from Pairs import deviation_matrix


def ts_plot(df,mat,data='B',trend=False):
    fig=plt.figure()
    ax0=fig.add_subplot(6,1,1)
    ax1=fig.add_subplot(6,1,2)
    ax2=fig.add_subplot(6,1,3)
    ax3=fig.add_subplot(6,1,4)
    ax4=fig.add_subplot(6,1,5)
    ax5=fig.add_subplot(6,1,6)
    if data=='B':
        if mat:
            if trend:
                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['CS'],1,full=True)
                ax0.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax0.set_xlabel('CS Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['CS'].max() - df['CS'].min()),2)))
                
                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['DB'],1,full=True)
                ax1.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax1.set_xlabel('DB Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['DB'].max() - df['DB'].min()),2)))
                
                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['GS'],1,full=True)
                ax2.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax2.set_xlabel('GS Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['GS'].max() - df['GS'].min()),2)))

                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['JPM'],1,full=True)
                ax3.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax3.set_xlabel('JPM Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['JPM'].max() - df['JPM'].min()),2)))
                
                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['MS'],1,full=True)
                ax4.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax4.set_xlabel('MS Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['MS'].max() - df['MS'].min()),2)))
                
                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['UBS'],1,full=True)
                ax5.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax5.set_xlabel('UBS Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['UBS'].max() - df['UBS'].min()),2)))
            if not trend:
                ax1.set_ylabel('DB Stock')
                ax2.set_ylabel('GS Stock')
                ax3.set_ylabel('JPM Stock')
                ax4.set_ylabel('MS Stock')
                ax5.set_ylabel('UBS Stock')

            ax0.plot(df['CS'].as_matrix())
            #ax0.set_ylabel('CS Stock')
            ax1.plot(df['DB'].as_matrix())
            #ax1.set_ylabel('DB Stock')
            ax2.plot(df['GS'].as_matrix())
            #ax2.set_ylabel('GS Stock')
            ax3.plot(df['JPM'].as_matrix())
            #ax3.set_ylabel('JPM Stock')
            ax4.plot(df['MS'].as_matrix())
            #ax4.set_ylabel('MS Stock')
            ax5.plot(df['UBS'].as_matrix())
            #ax5.set_ylabel('UBS Stock')
        else:
            ax0.plot(df['CS'])
            ax0.set_ylabel('CS Stock')
            ax1.plot(df['DB'])
            ax1.set_ylabel('DB Stock')
            ax2.plot(df['GS'])
            ax2.set_ylabel('GS Stock')
            ax3.plot(df['JPM'])
            ax3.set_ylabel('JPM Stock')
            ax4.plot(df['MS'])
            ax4.set_ylabel('MS Stock')
            ax5.plot(df['UBS'])
            ax5.set_ylabel('UBS Stock')
    elif data=='A':
        if mat:
            if trend:
                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['BAC'],1,full=True)
                ax0.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax0.set_xlabel('BAC Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['BAC'].max() - df['BAC'].min()),2)))

                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['C'],1,full=True)
                ax1.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax1.set_xlabel('C Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['C'].max() - df['C'].min()),2)))
                
                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['GS'],1,full=True)
                ax2.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax2.set_xlabel('GS Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['GS'].max() - df['GS'].min()),2)))
                
                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['JPM'],1,full=True)
                ax3.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax3.set_xlabel('JPM Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['JPM'].max() - df['JPM'].min()),2)))
                
                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['MS'],1,full=True)
                ax4.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax4.set_xlabel('MS Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['MS'].max() - df['MS'].min()),2)))
                
                coefficients, residuals, _, _, _ = np.polyfit(range(len(df.index)),df['WFC'],1,full=True)
                ax5.plot([coefficients[0]*x + coefficients[1] for x in range(len(df.index))])
                ax5.set_xlabel('WFC Stock, slope: '+str(np.round(coefficients[0],2)) +' nmse: '+str(np.round(np.sqrt(residuals[0]/(len(df.index)))/(df['WFC'].max() - df['WFC'].min()),2)))
            if not trend:
                ax0.set_ylabel('BAC Stock')
                ax1.set_ylabel('C Stock')
                ax2.set_ylabel('GS Stock')
                ax3.set_ylabel('JPM Stock')
                ax4.set_ylabel('MS Stock')
                ax5.set_ylabel('WFC Stock')

            ax0.plot(df['BAC'].as_matrix())
            #ax0.set_ylabel('BAC Stock')
            ax1.plot(df['C'].as_matrix())
            #ax1.set_ylabel('C Stock')
            ax2.plot(df['GS'].as_matrix())
            #ax2.set_ylabel('GS Stock')
            ax3.plot(df['JPM'].as_matrix())
            #ax3.set_ylabel('JPM Stock')
            ax4.plot(df['MS'].as_matrix())
            #ax4.set_ylabel('MS Stock')
            ax5.plot(df['WFC'].as_matrix())
            #ax5.set_ylabel('WFC Stock')
        else:
            ax0.plot(df['BAC'])
            ax0.set_ylabel('BAC Stock')
            ax1.plot(df['C'])
            ax1.set_ylabel('C Stock')
            ax2.plot(df['GS'])
            ax2.set_ylabel('GS Stock')
            ax3.plot(df['JPM'])
            ax3.set_ylabel('JPM Stock')
            ax4.plot(df['MS'])
            ax4.set_ylabel('MS Stock')
            ax5.plot(df['WFC'])
            ax5.set_ylabel('WFC Stock')
    else:
        print 'Dataset not available'
    plt.show()

def stationarity_plot(rho):
    df_stat=pd.Series(np.random.normal(0,1,500))
    df_int=pd.Series(np.cumsum(np.random.normal(0,1,500)))
    data=[]
    data.append(np.random.normal(0,1))
    for i in range(300):
        data.append(data[i]*rho+np.random.normal(0,1))
    df_stat_rho=pd.Series(data)
    plt.plot(df_stat,label='rho=0')
    plt.plot(df_int,label='rho=1')
    plt.plot(df_stat_rho,label='rho='.join(str(rho)))
    plt.legend()
    plt.show()

def trade_strategy_plot(rho_p=0.3,rho_t=0.99):
    fig=plt.figure()
    ax=fig.add_subplot(111)
    data=[]
    data.append(0)
    for i in range(400):
        data.append(data[i]*rho_p+np.random.normal(0,0.9))
    for i in range(100):
        data.append(data[i]*rho_t+np.random.normal(0,1))
    y=pd.DataFrame(data={'data':data})
    ax.plot(y,color='blue')
    #ax.plot(data[400:],color='blue')
    ax.axvline(400,color='r')
    low_pair,high_pair=np.percentile(data[:400],(5,95))
    ax.axhline(low_pair,color='r')
    ax.axhline(high_pair,color='r')
    ax.axhline(np.mean(data[:400]),color='r')
    ax.set_title('Example of pairs and trading residuals')
    plt.show()




def coint_example_plot(n,mu,sigma,a):
    s1 = np.random.normal(mu, sigma, n)
    s2 = np.random.normal(mu, sigma, n)
    x_1t = np.cumsum(s1)
    x_2t = a*np.cumsum(s1)+s2
    y = pd.DataFrame(data={'col1': x_1t, 'col2': x_2t} )
    fig=plt.figure()
    ax0=fig.add_subplot(211)
    ax0.set_title('Cointegrated time series')
    ax1=fig.add_subplot(212)
    ax0.plot(y)
    ax1.plot(np.cumsum(np.random.normal(mu,sigma,n)))
    ax1.plot(np.cumsum(np.random.normal(mu,sigma,n)))
    ax1.set_title('Two random walks')
    plt.show()

def plot_resample(df,path='C:\Users\Alexander Jaus\Documents\Uni\Bachelorarbeit\Data\CS\CS_2016-03-01_09-30-00-000047_2016-03-31_15-59-59-000916_odb.csv',start=pd.datetime(2016,03,01),end=pd.datetime(2016,03,2)):
    raw=pd.read_csv(path)
    raw['Price']=raw['Ask_Price']*0.5/10000+raw['Bid_Price']*0.5/10000
    raw['Time']=pd.to_datetime(raw['Time'])
    raw.set_index('Time',inplace=True)
    raw_log=np.log(raw['Price'])
    df_log=np.log(df)
    fig=plt.figure()
    ax=fig.add_subplot(111)
    ax.plot(raw_log[start:end],label='raw data')
    ax.plot(df_log['CS'][start:end],label='resampled')
    ax.set_title('Raw Data vs Resample')
    ax.set_xlabel('1st of March 2016')
    ax.set_ylabel('CS log Stock Price')
    plt.legend(loc=4)
    plt.show()

def arma_3x3_plot(ts_data,arma_fit,resids,label,start):
    fig=plt.figure()
    ax0=fig.add_subplot(311)
    ax0.set_ylabel('Time Series Data')
    ax0.set_xlabel('Johansen\'s residuals of '+label+' starting from '+str(start))
    ax1=fig.add_subplot(312)
    ax1.set_ylabel('Fitted Values')
    ax2=fig.add_subplot(313)
    ax2.set_ylabel('Residuals of ARMA(1,1)')
    ax0.plot(ts_data)
    ax1.plot(arma_fit)
    ax2.plot(resids)
    plt.show()
 
def plot_coint_robustness(df):
    fig=plt.figure()
    plt.title('All cointegration relationships for all possible tickers')
    subplots=[]
    cols=df.columns
    ncols=len(cols)
    for i in range(ncols):
        subplots.append(fig.add_subplot(ncols,1,int(i+1)))
        subplots[i].plot(df[cols[i]],marker='o')
        plt.yticks([0,1])
        subplots[i].set_ylabel(str(cols[i][0]+'\n'+cols[i][1]))
    plt.show()

def plot_periods(func,kind):
    def generate(function,int):
        for f in function:
            yield f[int]
    
    fig=plt.figure()
    ax1=fig.add_subplot(111, projection='3d')
    if kind=='bar':
        x_pos=[x for x in generate(func,0)]
        y_pos=[y for y in generate(func,1)]
        z_pos=np.zeros(len(x_pos))
        x_del=np.ones(len(x_pos))
        y_del=np.ones(len(y_pos))
        z_del=[z for z in generate(func,2)]
        ax1.bar3d(x_pos,y_pos,z_pos,x_del,y_del,z_del)
        ax1.set_xlabel('Formation Period')
        ax1.set_ylabel('Trade Period')
        ax1.set_zlabel('No stationary perdictions')
        plt.show()
    elif kind=='scatter':
        x=[x for x in generate(func,0)]
        y=[y for y in generate(func,1)]
        z=[z for z in generate(func,2)]
        ax1.scatter(x, y, z, c='g', marker='o')
        ax1.set_xlabel('Formation Period')
        ax1.set_ylabel('Trade Period')
        ax1.set_zlabel('No stationary perdictions')
        plt.show()
    elif kind=='contour':
        x=[x for x in generate(func,0)]
        y=[y for y in generate(func,1)]
        z=[z for z in generate(func,2)]
        x,y=np.meshgrid(x,y)
        ax1.plot_wireframe(x,y,z)
        ax1.set_xlabel('Formation Period')
        ax1.set_ylabel('Trade Period')
        ax1.set_zlabel('No stationary perdictions')
        plt.show()

    else:
        print kind+' is not a valid argument'

def normalized_prices_plot(df,tickers=['CS','DB']):
    fig=plt.figure()
    ax0=fig.add_subplot(2,1,1)
    ax0.set_ylabel('Stock Price in $') 
    ax0.set_xlabel('Number of Observations')
    ax1=fig.add_subplot(2,1,2)
    ax1.set_ylabel('Normalized Stock Price')
    ax1.set_xlabel('Number of Observations')
    start=pd.datetime(2016,03,1)
    end=pd.datetime(2016,03,7)
    normal=np.log(df).diff().dropna()
    for ticker in tickers:
        ax0.plot(df[ticker][start:end].as_matrix(),label=ticker)
        ax0.legend(loc=4)
        ax1.plot(normal[ticker][start:end].as_matrix(),label=ticker)
        ax1.legend(loc=1)
    plt.show()
 
def statistics_plot(resid_pairs,resid_trade,arma_pair,arma_trade,pair,start,evec,adfuller,mat=False,alpha=5,all_arma=None):
    label=str(pair)
    low_coint,high_coint=np.percentile(resid_pairs,(alpha,100-alpha))
    if all_arma is not None:
        low_pair,high_pair=np.percentile(all_arma,(alpha,100-alpha))
    else:
        low_pair,high_pair=np.percentile(arma_pair,(alpha,100-alpha))
        all_arma=arma_pair.append(arma_trade)

    fig=plt.figure()
    ax0=fig.add_subplot(1,1,1)
    if mat:
        #ax0.plot(resid_pairs.as_matrix())
        x=len(resid_pairs)
        #y=int(0.5*len(resid_trade))+x
        resid_pairs=resid_pairs.append(resid_trade)
        ax0.plot(resid_pairs.as_matrix(),color='blue',linestyle='-')
        ax0.axvline(x,color='k')
        #ax0.axvline(y,color='k')
        ax1=ax0.twinx()
        ax1.plot(all_arma.as_matrix(),color='red')
    else:
        ax0.plot(resid_pairs)
        ax0.plot(resid_trade)
    ax1.axhline(low_pair,color='r')
    ax1.axhline(high_pair,color='r')
    
    ax0.axhline(low_coint,color='b')
    ax0.axhline(high_coint,color='b')
    ax0.axhline(np.mean(resid_pairs),color='b')
    ax0.set_xlabel('Resids of '+str(label)+' starting from '+str(start)+' evec: '+str(np.round(evec,2))+' adfuller: '+str(np.round(adfuller,2)))
    plt.show()
    

if __name__=='__main__':
    
    df=pd.read_pickle('C:\Users\Alexander Jaus\Documents\Uni\Bachelorarbeit\Results\cdf_chong.pickle')
    