﻿import pandas as pd
import os

def convert_R_to_pickle(working_dir,file,pickle_name):
    path=working_dir
    data_name=file
    orig_df=pd.read_csv(os.path.join(path,data_name),sep=';')
    tickers=[x[x.rindex('.')+1:] for x in orig_df.columns[1:7]]

    #make entire index at first
    days=[]
    index_series=None
    delta_start=pd.Timedelta('9 hours 30 minutes')
    delta_ende=pd.Timedelta('15 hours 55 minutes')
    for col in orig_df.columns[1:]:
        date=pd.to_datetime(col[1:col.rindex('.')])
        if date not in days:
            days.append(date)
            if index_series is not None:
                index_series=index_series.append(pd.date_range(date+delta_start,date+delta_ende,freq='5 min'))
            elif index_series is None:
                index_series=pd.date_range(date+delta_start,date+delta_ende,freq='5 min')

    df=pd.DataFrame()
    for ticker in tickers:
        df[ticker]=None

    for ticker in tickers:
        indices=[i for i,s in enumerate(list(orig_df.columns)) if '.'+ticker in s]
        df[ticker]=pd.concat([orig_df[orig_df.columns[index]] for index in indices])
    df['index']=index_series
    df.set_index(keys='index',inplace=True)
    pd.to_pickle(df,os.path.join(path,pickle_name))


if __name__=='__main__':
    path='C:\Users\Alexander Jaus\Documents\Uni\Bachelorarbeit\Code_Samples'
    data_name='r_data.csv'

    