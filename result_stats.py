﻿import pandas as pd, numpy as np
import itertools, os
from graphs import plot_coint_robustness as plot_rob

def coint_robustness(data,results,index_of_dates=4,index_of_Pairs=2):
    tickers=data.columns
    possible_pairs=list(set(itertools.combinations(tickers,2)))
    start=data.first_valid_index().normalize()
    end=data.last_valid_index().normalize()
    all_dates=pd.date_range(start,end,freq='D')
    coint_dates=[x[index_of_dates] for x in results]
    coint_df=pd.DataFrame(0,index=all_dates,columns=possible_pairs)
    for date in all_dates:
        if date in all_dates:
            tickers=[x[index_of_Pairs] for x in results if x[index_of_dates]==date]
            for ticker in tickers:
                coint_df[tuple(ticker)][date]=1
    return coint_df

def evaluate_statistics(resid_pairs=None,resid_trading=None,alpha=5,all_arma=None,length=0):
    '''%evaluates statistic according to number of trading opportunitis 
    %under a given alpha as the number points above the percentiles
    %Returns amount of opprotunities to trade
    '''
    def classify(low,high,value):
        if value<low:
            return -1
        elif value>high:
            return 1
        else: 
            return 0
    if all_arma is not None:
        low_pair,high_pair=np.percentile(all_arma,(alpha,100-alpha))
        resid_pairs=all_arma[:length]
        resid_trading=all_arma[length:]
    else:
        low_pair,high_pair=np.percentile(resid_pairs,(alpha,100-alpha))
    trading_opportunities=0
    start_date=resid_trading.first_valid_index()
    #extract_series from Dataframe for convenience
    #first_resid_trading=resid_trading[0]
    first_resid_trading=resid_trading[start_date]
    #initiation
    movement=classify(low_pair,high_pair,first_resid_trading)
    for date in resid_trading.index[1:]:
        if movement!=classify(low_pair,high_pair,resid_trading[date]) and movement!=0:
            trading_opportunities+=1
        movement=classify(low_pair,high_pair,resid_trading[date])
    return trading_opportunities

def modified_robustness(rob_df,delta):
    '''Takes the dataframe reporesentation of robust dataframes as input 
    removes consequent entries smaller than the passend time delta.
    '''
    tickers=rob_df.columns
    dates=rob_df.index
    for ticker in tickers:
        movement=0
        last_ind=rob_df.last_valid_index()
        for date in dates:
            if movement==0 and rob_df[ticker][date]==1:
                movement=1
                start_date=date
            elif movement==1 and rob_df[ticker][date]==0:
                movement=0
                end_date=date
                if end_date-start_date<delta:
                    rob_df[start_date:end_date]=0
        if rob_df[ticker][last_ind]==1:
            start_date=last_ind
            while rob_df[ticker][start_date]==1:
                start_date=start_date-pd.Timedelta('1 days')
            if last_ind-start_date<delta:
                rob_df[start_date:last_ind]=0
    return rob_df
                
def convert_len_data_to_function(data,sep='='):
    '''Takes the string field for length_data and returns a puerly integer based return
    '''
    func=[]
    for r in data:
        x=int(r[0][r[0].index(sep)+len(sep):r[0].rindex(' ')])
        y=int(r[1][r[1].index(sep)+len(sep):r[1].rindex(' ')])
        z=int(r[2])
        func.append((x,y,z))
    return func

if __name__=='__main__':
    path='C:\Users\Alexander Jaus\Documents\Uni\Bachelorarbeit\Results'
    cdf_name='cdf_chong.pickle'
    cdf=pd.read_pickle(os.path.join(path,cdf_name))
    delta=pd.Timedelta('2 days')
    #mod_cdf=modified_robustness(cdf,delta)
    #plot_rob(cdf)