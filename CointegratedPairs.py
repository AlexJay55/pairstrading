﻿import pandas as pd, numpy as np, statsmodels as sm 
import os, itertools
from johansen import Johansen
from statsmodels.tsa.stattools import adfuller
from Pairs import constuct_statistics, evaluate_statistics
from graphs import statistics_plot as statplot
from result_stats import coint_robustness
from graphs import plot_coint_robustness as pcr
from graphs import ts_plot
from pandas.tseries.offsets import BDay
import matplotlib.pyplot as plt

def johansens_test(raw,trend=1,lags=3,alpha=0.05): #Use 2-3 lags, when dealing with a time frequency of 10 Minutes, according to BIC criteria
    '''% perform the johansens_test for given Data, it therby checks the Null Hypethesis of Johansen's test against n=0,1,2, with n being the 
    maximal number of co-integration relationships.
    trend: Int Variable diyplaying the trend
    lags. Int variable indicating the number of lags used to perform Johansen's Test
    '''
    converter={0.1:0,0.05:1,0.01:2}
    sig_level=converter[alpha]
    test=Johansen(raw,model=trend,k=lags,trace=True,significance_level=sig_level)
    try:
        evec,rej_r=test.johansen()
    except:
        return None
    if 0 in rej_r and 1 not in rej_r:
        print 'Yes, cointegrated'
        #raw_input('Wait')
        coint_vectors= evec[:,0]
        print coint_vectors
        return coint_vectors
    else:
        return None

def constuct_statistics(df,pair,start,pairing_days=10,trading_days=3,alpha=0.05):
    '''%Takes the df contining log-prices and the pair as a tuple of stocks and a given time frame.
    %Method checks, if pairs are co-integrated in the given time frame and consturcts an consturcts the pairs statistics
    '''
    pair_delta=BDay(pairing_days)
    end_pair=start+pair_delta
    trade_delta=BDay(trading_days)
    end_trade=end_pair+trade_delta
    pairs_data=df[pair][start:end_pair].copy()
    trade_data=df[pair][end_pair:end_trade].copy()
    if johansens_test(df[pair][start:end_pair],alpha=alpha) is None:
        #print str(pair)+ ' Do not have cointegrated prices in '+ str(start)+' till '+str(end_pair)
        return None,None,None
    else:
        evec=johansens_test(df[pair][start:end_pair],alpha=alpha)
        #There is at max 1 eigenvector
        evec=evec/evec[0] #eigenvector can be normalized
        #print pairs_data.head()
        print evec
        resid_pairs=pairs_data.dot(evec)
        resid_trading=trade_data.dot(evec)
        print len(resid_trading)
        return resid_pairs,resid_trading,evec

def get_arma(df):
    arma_model=sm.tsa.arima_model.ARMA(df,(1,1))
    arma_estimation=arma_model.fit(trend='c')
    rolling_average=arma_estimation.resid.rolling(7).mean()
    return arma_estimation,rolling_average
    

def make_coint_pairs(df,tickers,start,delta,alpha):
    '''Check any possible combination for cointegration
    '''
    end=start+delta
    possible_pairs=list(set(itertools.combinations(tickers,2)))
    pairs=[]
    for pair in possible_pairs:
        evec=johansens_test(df[list(pair)][start:end],alpha=alpha)
        if evec is not None and evec[0]*evec[1]<0:
            pairs.append(list(pair))
            print pair
    return pairs

def simulate_all_dates(df,percentiles=5,trade_period=3,formation_period=10,lags=3,critical_values=0.05,aduller_cval=0.1):
    '''%Input: df: Original Dataframe containing only prices
    %iterations: number of iterations
    % alpha: Confidence Interval
    %
    %Returns a list of valid results as a tuple of (resid_pairs,resid_trading,true_resid of pairs,pair,statistics_evaluation,start date)
    '''
    johansen_evecs=[]
    results=[]
    df=np.log(df)
    tickers=df.columns
    #The date range for pairs trading should not include the time which is needed to trade pairs
    trade_delta=BDay(trade_period)
    pair_delta=BDay(formation_period)
    dates=pd.date_range(df.index[0].date(),df.index[len(df.index)-1].date()-trade_delta-pair_delta,freq='B')
    for start in dates:
        end=start+pair_delta
        print 'Regarded time span: ' + str(start) + ' till ' + str(end)
        pairs=make_coint_pairs(df,tickers,start,pair_delta,critical_values)
        for pair in pairs:
            res_p,res_t,evec=constuct_statistics(df,pair,start,pairing_days=formation_period,trading_days=trade_period,alpha=critical_values)
            try:
                adfuller_p=adfuller(res_t,maxlag=lags,regression='c')[1]
                #Johansens Residuals of the trading period need to be stationary and on entry should be positiv, the other negative
                if adfuller_p<aduller_cval:
                    try:
                        #arma_res_p=get_arma(res_p)
                        #arma_res_t=get_arma(res_t)
                        arma_all,rolling_arma=get_arma(res_p.append(res_t))
                        #results.append((res_p,res_t,pair,adfuller_p,start,arma_res_p,arma_res_t,evec))
                        results.append((res_p,res_t,pair,adfuller_p,start,arma_all,evec,rolling_arma))
                        johansen_evecs.append(evec)
                    except:
                        pass
                else:
                    print 'The estimated relationship using the co-integration of the given vector are not stationary under p= '+str(adfuller_p)
            except ValueError as e:
                print e
    return results,johansen_evecs

def optimal_periods(df,critical_value=0.05):
    periods=[str(x)+' days' for x in range(30)]
    len_results=[]
    for f_period in periods:
        for t_period in periods[:periods.index(f_period)]:
            len_results.append(('formation: '+f_period,'trade: '+t_period,len(simulate_all_dates(df=df,trade_period=t_period,formation_period=f_period))))
    return len_results

if __name__=='__main__':
    path_orig='C:\Users\Alexander Jaus\Documents\Uni\Bachelorarbeit\Data'
    path_chong='C:\Users\Alexander Jaus\Documents\Uni\Bachelorarbeit\Code_Samples'
    data_name_chong='converted_R_data.pickle'
    data_name_orig='df_5min.pickle'
    df=pd.read_pickle(os.path.join(path_orig,data_name_orig))
    #ts_plot(df.resample('10 d').mean(),True,'A',True)
    results,evecs=simulate_all_dates(df,formation_period=10,trade_period=3,critical_values=0.05,aduller_cval=1)
    print 'Simulation Sucessful'
    if len(results)==0:
        print 'No co-integrated pair found'
    else:
        #for result in [x for i,x in enumerate(results) if evecs[i][0]*evecs[i][1]<0]:
        #    statplot(resid_pairs=result[0],resid_trade=result[1],pair=result[2],start=result[4],mat=True,alpha=10,arma_pair=result[5].resid,arma_trade=result[6].resid,adfuller=result[3],evec=result[6])
        
        for result in results[:5]:
            statplot(resid_pairs=result[0],resid_trade=result[1],pair=result[2],start=result[4],mat=True,alpha=10,adfuller=result[3],evec=result[6],all_arma=result[5].resid,arma_trade=None,arma_pair=None)