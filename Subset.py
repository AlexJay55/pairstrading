﻿import os 
import shutil

def createSubset(start,goal,size=100,):
    folders = [f for f in os.listdir(start) if not os.path.isfile(os.path.join(start,f))]
    try:
        for f in folders:
            os.makedirs(os.path.join(goal,f))
    except WindowsError:
        print 'Folders have already been generated'

    for f in folders:
        for data in os.listdir(os.path.join(start,f))[:size]:
            shutil.copyfile(os.path.join(start,f,data),os.path.join(goal,f,data))
        


if __name__=='__main__':
    createSubset(start='C:\Users\Alexander Jaus\Documents\Uni\Bachelorarbeit\Data',
        goal='C:\Users\Alexander Jaus\Documents\Uni\Bachelorarbeit\Data_Subset2')