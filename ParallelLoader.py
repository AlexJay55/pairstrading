﻿import numpy as np
import pandas as pd
import multiprocessing
from multiprocessing import Manager
import os

def get_stored_Data(path,tickers,start_date,end_date,datatype='.csv'):
    '''%Checks for already preprocessed data, considering path, stock tickers, date and datatype
    '''
    def file_relevant(file,start_date,end_date):
        #Returns true if the considered file has an overlap in the time period
        raw=file[[i for i, ltr in  enumerate(file) if ltr=='_'][0]+1:[i for i, ltr in  enumerate(file) if ltr=='_'][4]]
        start=pd.to_datetime(raw.split('_')[0]+'-'+raw.split('_')[1].replace('-',':',raw.split('_')[1].count('-')-1).replace('-','.'))
        end=pd.to_datetime(raw.split('_')[2]+'-'+raw.split('_')[1].replace('-',':',raw.split('_')[3].count('-')-1).replace('-','.'))
        if start_date>end or start>end_date:
            return False
        else:
            return True
    def delete_empty_tickers(data):
        delkeys=[]
        for d in data:
            if len(data[d][0])==len(data[d][1])==0:
                delkeys.append(d)
        for key in delkeys:
            del(data[key])
        return data
    
    data={}
        
    for ticker in tickers:
        data[ticker]=[[m for m in os.listdir(os.path.join(path,ticker)) if 'msg' in m and str(datatype) in m],
                        [o for o in os.listdir(os.path.join(path,ticker)) if 'odb' in o and str(datatype) in o]]

    #Delete empty keys in dictionary
    data=delete_empty_tickers(data)

    #Delete all data which is not relevant for time span
    for ticker in data:
        for aIndex in range(0,len(data[ticker])):
            #Need to be deleted in reverse, because of non-dynamic datatype
            for bIndex in range(len(data[ticker][aIndex])-1,-1,-1):
                if not file_relevant(data[ticker][aIndex][bIndex],start_date=start_date,end_date=end_date):
                    del(data[ticker][aIndex][bIndex])

    #Delete, empty tickers again
    data=delete_empty_tickers(data)
   
    return data

def get_tickers(path):
    '''%Returns a list of all tickers of which data is available for the given path
    '''
    return [ticker for ticker in os.listdir(path) if os.path.isdir(os.path.join(path,ticker))]

def load_ticker(messages,orderbooks,ticker,path,datatype,start_date,end_date,frequency,resample_how,data,odb_only):
    '''Internally Used Function to load the preprocessed data from a ticker
    '''
    pd_read_function={'.pickle': lambda arg:pd.read_pickle(arg),'.csv':lambda arg:pd.read_csv(arg)}   
    pd_resample_function={'first':lambda resampler:resampler.first(),'custom':lambda resampler,function:resampler.apply(function)}
    if not odb_only:
        messages[ticker]=pd.concat([pd_read_function[datatype](os.path.join(path,ticker,data[ticker][0][index])) 
                                    for index in range(0,len(data[ticker][0]))])
    orderbooks[ticker]=pd.concat([pd_read_function[datatype](os.path.join(path,ticker,data[ticker][1][index])) 
                                    for index in range(0,len(data[ticker][1]))])
    #Resample by taking the first in Time Interval
    if not odb_only:
        messages[ticker]['Time']=pd.to_datetime(messages[ticker]['Time'])
        messages[ticker].set_index('Time',inplace=True)
        messages[ticker]=messages[ticker][start_date:end_date]
        #No real interpretation when resampling messages, relationship of messages to orderbooks would be lost.
        #messages[ticker]=pd_resample_function[resample_how](messages[ticker].resample(frequency))
        #Handle Nan (only necessary in case of resampling)
        #There is no point to generate more  messages than there are, so drop them
        messages[ticker]=messages[ticker].dropna()

    orderbooks[ticker]['Time']=pd.to_datetime(orderbooks[ticker]['Time'])
    orderbooks[ticker].set_index('Time',inplace=True)
    orderbooks[ticker]=orderbooks[ticker][start_date:end_date]
    orderbooks[ticker]=pd_resample_function[resample_how](orderbooks[ticker].resample(frequency)) 
    #Fill the generated NaN Columns by forwardfilling
    orderbooks[ticker]=orderbooks[ticker].fillna(method='pad') 
    if odb_only:
        return None,orderbooks[ticker]
    else:
        return messages[ticker],orderbooks[ticker]

def get_Data(path,datatype,tickers,start_date,end_date,frequency='1S',resample_how='first',num_processes=4,del_old=True,odb_only=True):
    '''
    %Data handler function that checks for data availability, loads data, puts it together and resamples it. 
    %num_processes is a parameter specifiying how many processes shall be used in order to read and write the data,
    %If only orderbooks are needed, odb_only can be set to true, since it speedes up reading the files 
    %Notice: end date of time period is excluded
    '''
    def remove_old_Data(tickers):
        try:
            os.remove(os.path.join(path,'tickers'+datatype))
        except:
            pass

        for ticker in tickers:
            trash=[trash for trash in os.listdir(os.path.join(path,ticker)) if 'msg' in trash and datatype in trash or 'odb' in trash and datatype in trash]
            for t in trash:
                try:
                    os.remove(os.path.join(path,ticker,t))
                except:
                    pass
    
    def custom_resample(data): #Possibility to implement a custom resample function
        pass   
    data=get_stored_Data(path,tickers,start_date,end_date,datatype)
    print data
    raw_input('Press Key to Continue')
       
    if len(data)!=len(tickers) and del_old:
        print 'Old Data is being removed because of inconsistency'
        remove_old_Data(tickers)
        data=get_stored_Data(path=path,tickers=tickers,start_date=start_date,end_date=end_date,datatype=datatype)

    if len(data)==0:
        print 'No old Data has been found, try to load in data from directory: '+str(path)
        #Parallely read in Dataframes and concat them to new frames
        pool = multiprocessing.Pool(processes=num_processes)
        pool.map(write_Ticker,zip(tickers,[datatype]*len(tickers),[path]*len(tickers)))
        pool.close()
        pool.join()
        return get_Data(path,datatype,tickers,start_date=start_date,end_date=end_date,
                        frequency=frequency,resample_how=resample_how,del_old=False,odb_only=odb_only)
        
    else:
        print 'There is some old data which is trying to be loaded in'      
        messages={}
        orderbooks={}
        jobs=[]
        for ticker in tickers:
            messages[ticker],orderbooks[ticker]=load_ticker(messages,orderbooks,ticker,path,datatype,start_date,end_date,frequency,resample_how,data,odb_only)
        return messages,orderbooks

def write_Ticker((ticker,datatype,path)):
    '''Fuction which is internally called by the processes in the multithreading Pool in order to preprocess the data. 
    '''
    def make_file_name(ticker,type,df):
            return ticker+'_'+str(df.index[0]).replace(' ','_').replace(':','-').replace('.','-')+'_'+str(df.index[len(df.index)-1]).replace(' ','_').replace(':','-').replace('.','-')+'_'+type+datatype
            #Fomrat of file names: Ticker_y-m-d_h-m-s-ms_y-m-d_h-m-s-ms_type.datatype

    def get_Date(file):
        pos1=file.find('_')
        return np.array(file[pos1+1:file.find('_',pos1+1)].split('-')).astype('int')
    
    print ticker
    message_names=['Time','Type','ID','Size','Price','Direction']
    orderbook_names=['Ask_Price','Ask_Vol','Bid_Price','Bid_Vol']
    #Dictionary which allows picking a function according to an input parameter of the function
    pd_write_function={'.pickle': lambda obj,arg:pd.to_pickle(obj,arg),'.csv':lambda obj,arg:obj.to_csv(arg)}   

    get_time=lambda g:pd.Timedelta(seconds=g).components[1:6]
    get_stamp=lambda f:pd.datetime(year,month,day,*get_time(f)[:4])

    pairs=zip(sorted([m for m in os.listdir(os.path.join(path, ticker)) if 'message' in m]),
                sorted([o for o in os.listdir(os.path.join(path,ticker)) if 'orderbook' in o]))
    messages=None
    orderbooks=None        
    
    for pair in pairs:
        year, month, day = get_Date(pair[0])
        #Try to write monthly files
        if messages is not None and orderbooks is not None:
            new_df_message=pd.read_csv(os.path.join(path,ticker,pair[0]),header=None,names=message_names)
            new_df_message['Time']=new_df_message['Time'].apply(get_stamp)
            try:
                if month!=pd.to_datetime(messages.head(1)['Time'].values[0]).month:
                    raise MemoryError
                messages=messages.append(new_df_message)
            except MemoryError:
                #In case of a memory error, the current process is stopped, the data is written on the harddrive and starting from there a new file beginns 
                messages.set_index('Time',inplace=True)
                pd_write_function[datatype](messages,os.path.join(path,ticker,make_file_name(ticker,'msg',messages)))
                del(messages)
                messages=new_df_message
                   
            new_df_orderbook=pd.read_csv(os.path.join(path,ticker,pair[1]),header=None,names=orderbook_names)
            new_df_orderbook['Time']=new_df_message['Time']
            #critical Process to append frames, considering the computuational resources
            try:
                if month!=pd.to_datetime(orderbooks.head(1)['Time'].values[0]).month:
                    raise MemoryError
                orderbooks=orderbooks.append(new_df_orderbook)
            except MemoryError: 
                orderbooks.set_index('Time',inplace=True)
                pd_write_function[datatype](orderbooks,os.path.join(path,ticker,make_file_name(ticker,'odb',orderbooks)))
                del(orderbooks)
                orderbooks=new_df_orderbook
        #Called during the first run
        elif messages is None and orderbooks is None:
            messages=pd.read_csv(os.path.join(path,ticker,pair[0]),header=None,names=message_names)
            messages['Time']=messages['Time'].apply(get_stamp)
            orderbooks=pd.read_csv(os.path.join(path,ticker,pair[1]),header=None,names=orderbook_names)
            orderbooks['Time']=messages['Time']
            
        else:
            print 'Somethin unusual happend during the pairing of the files'


    messages.set_index('Time',inplace=True)
    orderbooks.set_index('Time',inplace=True)
            
            
    pd_write_function[datatype](messages,os.path.join(path,ticker,make_file_name(ticker,'msg',messages)))
    pd_write_function[datatype](orderbooks,os.path.join(path,ticker,make_file_name(ticker,'odb',orderbooks)))
            
    del(messages)
    del(orderbooks)

if __name__=='__main__':
    path='C:\Users\Alexander Jaus\Documents\Uni\Bachelorarbeit\Data'
    tickers=get_tickers(path)
    start_date=pd.datetime(2016,05,11)
    end_date=pd.datetime(2016,05,16)

    messages, orderbooks = get_Data(path=path,datatype='.csv',tickers=['CS'],start_date=start_date,
                                    end_date=end_date,frequency='1S',resample_how='first',num_processes=2,del_old=False)
    
    